provider "aws" {
  region = "ap-south-1"
  profile= "default"

}

resource "aws_instance" "myinstance" {
  ami = "ami-02e60be79e78fef21"
  instance_type = "t2.micro"
  subnet_id = "subnet-34e38078"
  tags = {
    Name = "jenkins-terra"
  }
  security_groups = ["sg-0ec25911809d38ee5"]
  key_name = "centos"
}
